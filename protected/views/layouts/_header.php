<?php 
$criteria = new CDbCriteria;
$criteria->addCondition('active = "1"');
$criteria->order = 't.date_input ASC';
$mod_kategh = ViewGallery::model()->findAll($criteria);
?>

<header class="head headers fixed-top <?php if ($active_menu_pg == 'home/index' or $active_menu_pg == 'home/abouthistory' or $active_menu_pg == 'home/aboutquality' or $active_menu_pg == 'home/aboutcareer'): ?>homes_head<?php endif ?> ">
  <div class="prelative container d-none d-sm-block">
      <div class="row no-gutters">
        <div class="col-md-15">
          <div class="logo_heads"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img src="<?php echo $this->assetBaseurl ?>new/logo-head.png" alt="" class="img img-fluid"></a></div>
        </div>
        <div class="col-md-30">
          <div class="text-center menu-top">
            <ul class="list-inline">
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">HOME</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">COMPANY</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'lang'=>Yii::app()->language)); ?>">PRODUCTS</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/quality', 'lang'=>Yii::app()->language)); ?>">QUALITY</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/gallery', 'lang'=>Yii::app()->language)); ?>">GALLERY</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>">CONTACT</a></li>
            </ul>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-15">
          <div class="text-right dnx_language">
            <?php
            $get['lang'] = 'id';
            ?>
            <a href="<?php echo $this->createUrl($this->route, $get) ?>"><img src="<?php echo $this->assetBaseurl ?>new/lang-flag_in.jpg" alt="" class="img img-fluid"></a>
            &nbsp;&nbsp;&nbsp;
            <?php
            $get['lang'] = 'en';
            ?>
            <a href="<?php echo $this->createUrl($this->route, $get) ?>"><img src="<?php echo $this->assetBaseurl ?>new/lang-flag_en.jpg" alt="" class="img img-fluid"></a>
          </div>
        </div>
      </div>
      <div class="clear"></div>
  </div>
 
  <div class="d-block d-sm-none">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
        <img src="<?php echo $this->assetBaseurl ?>logo-headers.png" style="max-width: 145px;" alt="" class="img img-fluid">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">HOME</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">COMPANY</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'lang'=>Yii::app()->language)); ?>">PRODUCTS</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/quality', 'lang'=>Yii::app()->language)); ?>">QUALITY</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/gallery', 'lang'=>Yii::app()->language)); ?>">GALLERY</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>">CONTACT</a></li>
        </ul>
      </div>
    </nav>
  </div>
</header>

<?php
/*
<div class="outer-blok-black-menuresponss-hides d-none">
  <div class="prelatife container">
    <div class="clear height-45"></div>
    <div class="fright">
      <div class="hidesmenu-frightd"><a href="#" class="closemrespobtn"><img src="<?php echo $this->assetBaseurl ?>closen-btn.png" alt=""></a></div>
    </div>
    <div class="py-3"></div>
    <div class="blocksn_logo-centers d-block mx-auto text-center">
      <img src="<?php echo $this->assetBaseurl ?>logo-nippo.svg" style="max-width: 167px;" alt="logo" class="img img-fluid mx-auto">
    </div>
    <div class="py-4"></div>
    <div class="menu-sheader-datals">
      <ul class="list-unstyled">
        <?php foreach ($mod_kategh as $key => $value): ?>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/dining', 'id'=>$value->id, 'name'=> strtolower($value->title))); ?>"><?php echo ucwords(strtolower($value->title)); ?></a></li>       
        <?php endforeach ?>
      </ul>
      <div class="py-2"></div>
      <div class="py-1"></div>
      <div class="line-separate d-block mx-auto"></div>
      <div class="py-2"></div>
      <ul class="list-unstyled n_menu2">
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Us</a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/quality')); ?>">Our Quality</a></li>
        <li><a href="#">Nippo Profile Book</a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact Us</a></li>
      </ul>
      <div class="py-2"></div>
      <div class="line-separate d-block mx-auto"></div>
      <div class="py-1"></div>
      <div class="py-2"></div>
    </div>
    <div class="blocks-info-menubtm text-center">
      <span>Social Media</span>
      <div class="py-1"></div>
      <ul class="list-inline">
        <li class="list-inline-item"><a target="_blank" href="<?php echo $this->setting['url_instagram'] ?>">Instagram</a></li>
        <li class="list-inline-item">|</li>
        <li class="list-inline-item"><a target="_blank" href="<?php echo $this->setting['url_youtube'] ?>">Youtube</a></li>
      </ul>
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>
  <div class="clear"></div>
</div>

<script type="text/javascript">
  $(function(){
    // show and hide menu responsive
    $('a.showmenu_barresponsive').on('click', function() {
      $('.outer-blok-black-menuresponss-hides').slideToggle('slow');
      return false;
    });
    $('a.closemrespobtn').on('click', function() {
      $('.outer-blok-black-menuresponss-hides').slideUp('slow');
      return false;
    });

  })
</script>
*/ 
?>