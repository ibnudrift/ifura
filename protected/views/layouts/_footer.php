<footer class="foot">
    <div class="py-5"></div>
    <div class="py-2"></div>
    <div class="prelatife container">
        <div class="inners text-center">
            <div class="row">
                <div class="col">
                    <div class="lgo_footer">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>"><img src="<?php echo $this->assetBaseurl.'new/lgo-ifuras-footers.png' ?>" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid"></a>
                    </div>
                </div>
            </div>
            <div class="py-3"></div>
            <div class="sub-footer-menu">
                <ul class="list-inline m-0">
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">HOME</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>">COMPANY</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'lang'=>Yii::app()->language)); ?>">PRODUCTS</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/quality', 'lang'=>Yii::app()->language)); ?>">QUALITY</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/gallery', 'lang'=>Yii::app()->language)); ?>">GALLERY</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang'=>Yii::app()->language)); ?>">CONTACT</a></li>
                </ul>
            </div>

            <div class="py-3"></div>
            <div class="lines-grey"></div>
            <div class="py-3"></div>

            <div class="t-copyrights">
                <p>Copyright &copy; 2020 - PT. Indo Furnitama Raya. All rights reserved. Website design & development by <a title="Website Design Surabaya" href="https://www.markdesign.net/">Mark Design Indonesia</a>.</p>
            </div>
                
            </div>
            <div class="clear clearfix"></div>
        </div>
    </div>
</footer>
<?php
$wa_number = str_replace('+', '', str_replace(' ', '', $this->setting['contact_wa'])) ;
?>
<section class="live-chat">
    <div class="row">
        <div class="col-md-60">
            <div class="live">
                <a href="https://wa.me/<?php echo $wa_number; ?>">
                    <img src="<?php echo $this->assetBaseurl; ?>Whatsapp-Click-to-chat.png" class="img img-fluid" alt="">
                </a>
            </div>
        </div>
    </div>
</section>

<section class="nfoot_mob_wa d-block d-sm-none">
    <div class="row">
        <div class="col-md-60">
            <div class="nx_button">
                <a href="https://wa.me/<?php echo $wa_number; ?>"><i class="fa fa-whatsapp"></i> Whatsapp</a>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    .imgs_vold img{
        max-width: 255px;
    }
    .live-chat .live img{
        /*max-width: 175px;*/
    }
    section.live-chat{
        display: none;
        z-index: 150;
    }
    
    
    @media (min-width: 1280px){
        .cover-insides .pictures_all img{
            width: 100%;
        }
    }
    .cover-insides .pictures_all img{
        width: 100%;
    }
</style>