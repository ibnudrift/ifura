<div class="leftmenu">        
    <ul class="nav nav-tabs nav-stacked">
        <li class="nav-header">Navigation</li>
        
        <li class="dropdown"><a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Products') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/product')); ?>">Static Product</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/product')); ?>">View Product</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/create')); ?>">Add Product</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/category')); ?>">Category List</a></li>
            </ul>
        </li>

        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>"><span class="fa fa-camera"></span> <?php echo Tt::t('admin', 'Slides') ?></a></li>
        <li>&nbsp;</li>
        
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/homepage')); ?>"><span class="fa fa-file"></span> <?php echo Tt::t('admin', 'Home') ?></a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/about')); ?>"><span class="fa fa-file"></span> <?php echo Tt::t('admin', 'About Us') ?></a></li>
        

        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/quality')); ?>"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Our Quality') ?></a></li>


        <li class="dropdown"><a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Gallery') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/career')); ?>">Static Gallery</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/gallery')); ?>">Data Gallery</a></li>
                <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/tcareer')); ?>">View Career List</a></li> -->
            </ul>
        </li>

        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/contact')); ?>"><span class="fa fa-phone"></span> <?php echo Tt::t('admin', 'Contact Us') ?></a></li>
        
        <li><a href="<?php echo CHtml::normalizeUrl(array('setting/index')); ?>"><span class="fa fa-cogs"></span> <?php echo Tt::t('admin', 'General Setting') ?></a>
        </li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/home/logout')); ?>"><span class="fa fa fa-sign-out"></span> Logout</a></li>
    </ul>
</div><!--leftmenu-->
