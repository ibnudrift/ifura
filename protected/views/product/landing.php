<section class="cover-insides prelatife">
  <div class="pictures_all wow fadeInDown">
    <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['product_hero_image']; ?>" alt="" class="img img-fluid">
  </div>
  <div class="inners_cover wow fadeInDown">   
    <div class="inners_cvr">
      <div class="row">
        <div class="col-md-60">
          <div class="texts text-center">
            <h1><?php echo $this->setting['product_hero_title'] ?></h1>
            <div class="py-2"></div>
            <div class="lines-separator-mid"></div>
            <div class="py-2"></div>
            <p><?php echo $this->setting['product_hero_subtitle'] ?></p>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>

<section class="contact_outer_content back-white">
  <div class="prelatife container">
    <div class="py-5"></div>
    <div class="py-3 d-none d-sm-block"></div>

    <div class="content-text text-center">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-54">
          <h2>IFURA WOOD PRODUCTS</h2>
          <p>We are now in the era where precision is a luxury, IFURA has the reputation for that. IFURA is not here just to offer wood manufacturing and wood working products, instead we offer solutions and creations for new trends of designs and efficiencies. All IFURA products will come with both high durability and reliability, thanks to our high-precision machineries and our strict quality control these will be the best products to gain your business’ profit. </p>
          <p><strong>Please browse of our general product line below.</strong></p>
        </div>
        <div class="col-md-3"></div>
      </div>

      <div class="py-4"></div>
      <div class="clear"></div>

      <div class="outer_content_products">
        <h3>IFURA WOOD PRODUCTS</h3>
        <div class="py-3"></div>
        <?php if ( is_array($product) and count($product) > 0): ?>
          <?php foreach ($product as $ke => $val): ?>
          <div class="text-center tp_title_cat">
            <h4><strong><?php echo $val['name'] ?></strong></h4>
          </div>
          <div class="py-3"></div>
          <?php if ($val['sublist']): ?>

            <?php foreach ($val['sublist'] as $ke_sublist => $va_sublist): ?>
            <div class="text-center tp_title_cat">
              <h4><small><strong><?php echo $va_sublist['name'] ?></strong></small></h4>
            </div>
            <div class="py-2"></div>
            <!-- start view product -->
            <div class="lists_prd_default">
              <div class="row justify-content-center">
                <?php foreach ($va_sublist['list_product'] as $key2 => $value2): ?>
                <?php
                $value2 = json_decode( json_encode($value2) );
                ?>
                <div class="col-md-15">
                  <div class="boxed">
                    <div class="picture no_shadow">
                      <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value2->id, 'slug'=>$value2->name, 'lang'=>Yii::app()->language)); ?>">
                        <img src="<?php echo Yii::app()->baseUrl. '/images/product/'. $value2->image; ?>" alt="<?php echo $value2->name ?>" class="picts_ifr <?php echo ($value2->name == 'Lignum Doors')? 'maw100': '' ?>">
                      </a>
                    </div>
                    <div class="info">
                      <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value2->id, 'slug'=>$value2->name, 'lang'=>Yii::app()->language)); ?>"><span><?php echo $value2->name ?></span></a>
                    </div>
                  </div>
                </div>
                <?php endforeach ?>
              </div>
            </div>
            <!-- end view product -->
            <div class="py-3"></div>
            <?php endforeach ?>
            <!-- end sublist -->

          <?php else: ?>

          <!-- // list langsung             -->
          <?php if ($val['list']): ?>
          <div class="lists_prd_default">
            <div class="row justify-content-center">
              <?php foreach ($val['list'] as $key => $value): ?>
              <?php
              $value = json_decode( json_encode($value) );
              ?>
              <div class="col-md-15 col-30">
                <div class="boxed">
                  <div class="picture">
                    <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value->id, 'slug'=>$value->name, 'lang'=>Yii::app()->language)); ?>">
                      <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(250,234, '/images/product/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="<?php echo $value->name ?>" class="img img-fluid">
                    </a>
                  </div>
                  <div class="info">
                    <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value->id, 'slug'=>$value->name, 'lang'=>Yii::app()->language)); ?>"><span><?php echo $value->name ?></span></a>
                  </div>
                </div>
              </div>
              <?php endforeach ?>
            </div>
          </div>
          <?php else: ?>
            <p><strong>Sorry, Data is empty!</strong></p>
          <?php endif ?>
        <?php endif ?>
        <!-- end list langsung -->
          <div class="py-4"></div>
          <?php endforeach ?>
        <?php endif ?>
        


        <div class="clear"></div>
      </div>

      <div class="clear"></div>
    </div>

    <div class="py-5"></div>
  </div>
</section>

<style type="text/css">
  img.picts_ifr{
    /*max-height: 490px;*/
    max-height: 625px;
    display: block;
    margin: 0 auto;
    webkit-box-shadow: 4px 4px 10px 0 rgba(50,50,50,0.38);
    -moz-box-shadow: 4px 4px 10px 0 rgba(50,50,50,0.38);
    box-shadow: 4px 4px 10px 0 rgba(50,50,50,0.38);
  }
  img.picts_ifr.maw100{
    max-width: 100%;
  }
  .picture.no_shadow{
    background-color: transparent !important;
    box-shadow: none !important;
    display: block;
    margin: 0 auto; text-align: center;
  }
</style>