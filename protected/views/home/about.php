
<section class="cover-insides prelatife">
  <div class="pictures_all wow fadeInDown">
    <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['about_hero_image']; ?>" alt="" class="img img-fluid">
  </div>
	<div class="inners_cover wow fadeInDown">		
    <div class="inners_cvr">
			<div class="row">
        <div class="col-md-60">
          <div class="texts text-center">
            <h1><?php echo $this->setting['about_hero_title'] ?></h1>
            <div class="py-2"></div>
            <div class="lines-separator-mid"></div>
            <div class="py-2"></div>
            <p><?php echo $this->setting['about_hero_subtitle'] ?></p>
          </div>
        </div>
			</div>
		</div>

	</div>
</section>

<section class="about_outer_content back-white">
  <div class="prelatife container">
    <div class="py-5"></div>
    <div class="py-3 d-none d-sm-block"></div>
    
    <div class="content-text text-center">

       <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-54">
          <?php echo $this->setting['about1_content'] ?>
        </div>
        <div class="col-md-3"></div>
      </div>

      <div class="py-3"></div>
      <div class="row">
        <div class="col-md-20">
          <div class="pictures"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(345,236, '/images/static/'. $this->setting['about1_picturesn_1'] , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="" class="img img-fluid w-100"></div>
          <div class="d-block d-sm-none py-2"></div>
        </div>
        <div class="col-md-20">
          <div class="pictures"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(345,236, '/images/static/'. $this->setting['about1_picturesn_2'] , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="" class="img img-fluid w-100"></div>
          <div class="d-block d-sm-none py-2"></div>
        </div>
        <div class="col-md-20">
          <div class="pictures"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(345,236, '/images/static/'. $this->setting['about1_picturesn_3'] , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="" class="img img-fluid w-100"></div>
          <div class="d-block d-sm-none py-2"></div>
        </div>
      </div>
      <div class="py-3"></div>
  
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-54">
          <?php echo $this->setting['about1_content2'] ?>
        </div>
        <div class="col-md-3"></div>
      </div>
      

      <div class="py-3 my-1"></div>
      <img class="img img-fluid" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['asfasdf'] ?>" alt="">
      <div class="py-2"></div>

      <div class="clear"></div>
    </div>

    <div class="py-5 d-none d-sm-block"></div>
    <div class="py-3 d-block d-sm-none"></div>
  </div>
</section>

<section class="about_outer_content2 back-white">
  <div class="prelatife container">
    <div class="py-5"></div>
    
    <div class="content-text text-center d-block mx-auto mw950">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-54">
          <?php echo $this->setting['about2_content'] ?>
        </div>
        <div class="col-md-3"></div>
      </div>

      <div class="clear"></div>
    </div>

  </div>
</section>

<section class="blocks_about_vission py-5">
  <div class="prelatife container py-4">
    
    <div class="content-text">
      <div class="row">
        <div class="col-md-30">
          <div class="boxed_text">
            <h3>VISION</h3>
            <?php echo $this->setting['about3_visi'] ?>
          </div>
        </div>
        <div class="col-md-30 border_left">
          <div class="boxed_text">
            <h3>MISSION</h3>
            <?php echo $this->setting['about3_mission'] ?>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>

<section class="about_outer_content4">
  <div class="prelatife container">
    <div class="py-5"></div>
    
    <div class="content-text text-center d-block mx-auto mw950">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-54">
          <?php echo $this->setting['about4_contents'] ?>
        </div>
        <div class="col-md-3"></div>
      </div>
      <div class="py-2"></div>
      <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['about4_picture_map'] ?>" alt="" class="img img-fluid">
      <!-- <div class="py-5 d-none d-sm-block"></div> -->
      <div class="py-2 d-block d-sm-none"></div>

      <div class="clear"></div>
    </div>
  </div>
  <div class="d-block d-sm-none banners_bottom_full">
    <img src="<?php echo $this->assetBaseurl.'new/back-about-containerloading_mob.jpg' ?>" alt="" class="img img-fluid">
  </div>
</section>