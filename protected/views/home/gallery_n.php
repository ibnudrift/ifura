<section class="cover-insides prelatife">
  <div class="pictures_all wow fadeInDown">
    <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['career_hero_image']; ?>" alt="" class="img img-fluid">
  </div>
  <div class="inners_cover wow fadeInDown">   
    <div class="inners_cvr">
      <div class="row">
        <div class="col-md-60">
          <div class="texts text-center">
            <h1><?php echo $this->setting['career_hero_title'] ?></h1>
            <div class="py-2"></div>
            <div class="lines-separator-mid"></div>
            <div class="py-2"></div>
            <p><?php echo $this->setting['career_hero_subtitle'] ?></p>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>

<section class="career_outer_content back-white">
  <div class="prelatife container">
    <div class="py-5"></div>
    <div class="py-3"></div>

    <div class="text-center lists_def_gallery_data">
      <?php if ($gallery): ?>
      <div class="row justify-content-center text-center">
        <?php foreach ($gallery->getData() as $key => $value): ?>
        <?php 
        $m_gallery_image = GalleryImage::model()->findAll('gallery_id = :gal_id', array(':gal_id'=>$value->id));
        ?>
        <div class="col-md-15 col-30">
          <div class="items">
            <div class="picture">
              <div class="aniimated-thumbnials">
                <a data-sub-html="<?php echo $value->title ?>" href="<?php echo Yii::app()->baseUrl.'/images/gallery/'. $value->image ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(410,410, '/images/gallery/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="" class="img img-fluid"></a>
                <?php if ($m_gallery_image): ?>
                  <?php foreach ($m_gallery_image as $key => $val): ?>
                  <a data-sub-html="<?php echo $value->title ?>" href="<?php echo Yii::app()->baseUrl.'/images/gallery/'. $val->image ?>"></a>
                  <?php endforeach ?>
                <?php endif ?>
              </div>
            </div>
            <div class="info py-3">
              <div class="aniimated-thumbnials">
                <a data-sub-html="<?php echo $value->title ?>" href="<?php echo Yii::app()->baseUrl.'/images/gallery/'. $value->image ?>"><h4><?php echo $value->title ?></h4></a>
                <?php if ($m_gallery_image): ?>
                    <?php foreach ($m_gallery_image as $key => $val): ?>
                    <a data-sub-html="<?php echo $value->title ?>" href="<?php echo Yii::app()->baseUrl.'/images/gallery/'. $val->image ?>"></a>
                    <?php endforeach ?>
                  <?php endif ?>
                </div>
            </div>
          </div>
        </div>
        <?php endforeach ?>
      </div>
      <?php endif ?>

    </div>

    <div class="py-5"></div>
  </div>
</section>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.7.2/js/lightgallery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.7.2/css/lightgallery.min.css">
<script type="text/javascript">
  $(function(){
    $('.aniimated-thumbnials').lightGallery({
        thumbnail:false
    }); 
  });
</script>