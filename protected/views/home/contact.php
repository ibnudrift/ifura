<section class="cover-insides prelatife">
  <div class="pictures_all wow fadeInDown">
    <img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['contact_hero_image']; ?>" alt="" class="img img-fluid">
  </div>
  <div class="inners_cover wow fadeInDown">   
    <div class="inners_cvr">
      <div class="row">
        <div class="col-md-60">
          <div class="texts text-center">
            <h1><?php echo $this->setting['contact_hero_title'] ?></h1>
            <div class="py-2"></div>
            <div class="lines-separator-mid"></div>
            <div class="py-2"></div>
            <p><?php echo $this->setting['contact_hero_subtitle'] ?></p>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>

<section class="contact_outer_content back-white">
  <div class="prelatife container">
    <div class="py-5"></div>
    <div class="py-3 d-none d-sm-block"></div>

    <div class="content-text text-center">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-54">
          <?php echo $this->setting['contact_content_top'] ?>
        </div>
        <div class="col-md-3"></div>
      </div>

      <div class="py-4"></div>
      
      <div class="row">
        <div class="col-md-10"></div>
        <div class="col-md-40">
          <div class="row call-info">
            <div class="col">
              <div class="box_text">
                <i class="fa fa-envelope-o"></i>
                <div class="py-2"></div>
                <p>Email <br><a href="mailto:<?php echo $this->setting['contact_email'] ?>"><?php echo $this->setting['contact_email'] ?></a></p>
              </div>
            </div>
            <div class="col border-left">
              <div class="box_text">
                <i class="fa fa-whatsapp"></i>
                <div class="py-2"></div>
                <?php
                $wa_number = str_replace('+', '', str_replace(' ', '', $this->setting['contact_wa'])) ;
                ?>
                <p>Whatsapp / Phone<a href="https://wa.me/<?php echo $wa_number; ?>" target="_blank"><br><?php echo $this->setting['contact_wa'] ?></a><br>(CLICK TO CHAT)</p>
              </div>
            </div>
            <div class="col border-left">
              <div class="box_text">
                <i class="fa fa-users"></i>
                <div class="py-2"></div>
                <p>Job Opportunity<a href="mailto:<?php echo $this->setting['contact_email_career'] ?>"><br><?php echo $this->setting['contact_email_career'] ?></a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-10"></div>
      </div>

      <div class="py-4"></div>
      <div class="d-none d-sm-block py-3"></div>

      <div class="row">
        <div class="col-md-20">
          <img class="img img-fluid w-100" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['banner_contact_pict_t1'] ?>" alt="">
          <div class="d-block d-sm-none py-2"></div>
        </div>
        <div class="col-md-20">
          <img class="img img-fluid w-100" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['banner_contact_pict_t2'] ?>" alt="">
          <div class="d-block d-sm-none py-2"></div>
        </div>
        <div class="col-md-20">
          <img class="img img-fluid w-100" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['banner_contact_pict_t3'] ?>" alt="">
        </div>
      </div>

      <div class="d-none d-sm-block py-5"></div>
      <div class="d-block d-sm-none py-4 my-2"></div>

      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-54">
         <?php echo $this->setting['contact_content_middle'] ?>
        </div>
        <div class="col-md-3"></div>
      </div>

      <div class="clear"></div>
    </div>

    <div class="d-none d-sm-block py-5"></div>
    <div class="d-block d-sm-none py-4"></div>
  </div>
</section>