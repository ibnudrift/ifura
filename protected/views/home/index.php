<section class="home-section-1 back-wood py-5">
    <div class="prelatife container">
        <div class="inners py-5">

            <div class="top-text text-center mx-auto content-text">
                <h2><?php echo $this->setting['home_section1_titles'] ?></h2>
                <?php echo $this->setting['home_section1_content'] ?>
            </div>
            <div class="py-4"></div>
            <div class="row">
                <div class="col-20">
                    <div class="boxed-text text-center">
                        <img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['home_section1_images_1'] ?>" alt="" class="img img-fluid d-block mx-auto">
                        <div class="py-2"></div>
                        <?php echo $this->setting['home_section1_titlesn_1'] ?>
                    </div>
                </div>
                <div class="col-20">
                    <div class="boxed-text text-center">
                        <img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['home_section1_images_2'] ?>" alt="" class="img img-fluid d-block mx-auto">
                        <div class="py-2"></div>
                        <?php echo $this->setting['home_section1_titlesn_2'] ?>
                    </div>
                </div>
                <div class="col-20">
                    <div class="boxed-text text-center">
                        <img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['home_section1_images_3'] ?>" alt="" class="img img-fluid d-block mx-auto">
                        <div class="py-2"></div>
                        <?php echo $this->setting['home_section1_titlesn_3'] ?>
                    </div>
                </div>
                
            </div>

            <div class="clear clearfix"></div>
        </div>
    </div>
</section>

<section class="home-section-2 back-white py-5">
    <div class="prelatife container">
        <div class="inners py-5">

            <div class="in_content content-text text-center">
                <h2><?php echo $this->setting['home_section2_title'] ?></h2>
                <div class="py-2 d-none d-sm-block"></div>
                <?php echo $this->setting['home_section2_content'] ?>
            </div>
            <div class="py-4"></div>
            <div class="ountings_middle_products text-center">
                <h3>OUR PRODUCTS</h3>
                <?php 
                    $criteria = new CDbCriteria;
                    $criteria->with = array('description');
                    $criteria->addCondition('t.parent_id = :parents_id');
                    $criteria->params[':parents_id'] = 0;
                    $criteria->addCondition('t.type = :type');
                    $criteria->params[':type'] = 'category';
                    $criteria->order = 'sort ASC';
                    $data_ncategory = PrdCategory::model()->findAll($criteria);
                ?>
                <div class="py-3"></div>
                <div class="lists_pictures_circle">
                    <div class="row justify-content-center">
                        <?php foreach ($data_ncategory as $key => $value): ?>
                        <div class="col-md-15 col-30">
                            <div class="box-item">
                                <div class="d-block picture">
                                    <a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=> $value->id, 'lang'=>Yii::app()->language)); ?>">
                                    <img src="<?php echo Yii::app()->baseUrl.'/images/category/'. $value->image ?>" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid d-block mx-auto" style="max-width: 153px;">
                                    </a>
                                </div>
                                <div class="py-2"></div>
                                <a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=> $value->id,'lang'=>Yii::app()->language)); ?>">
                                <h5><?php echo strtoupper($value->description->name) ?></h5>
                                </a>
                            </div>
                        </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
            <div class="py-2"></div>

        </div>
    </div>
</section>

<section class="fullscreen_andtext_home py-5">
    <div class="inner_text my-5">
        <div class="maw600 mx-auto d-block py-5 my-5">
        <?php echo $this->setting['home_section3_content'] ?>
        </div>
    </div>
</section>
